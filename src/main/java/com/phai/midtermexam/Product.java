/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phai.midtermexam;

import java.io.Serializable;

/**
 *
 * @author Phai
 */
public class Product implements Serializable{
    private String productName;
    private String brandName;
    private String productPrice;
    private String productAmount;
    
    public Product(String productName,String brandName,
            String productPrice,String productAmount){
        this.productName = productName;
        this.brandName = brandName;
        this.productPrice = productPrice;
        this.productAmount = productAmount;
    }
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(String productAmount) {
        this.productAmount = productAmount;
    }

    @Override
    public String toString() {
        return  "Name=" + productName + ", Brand=" + brandName + ", "
                + "Price=" + productPrice + ", Amount=" + productAmount ;
    }
    
}
