/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phai.midtermexam;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Phai
 */
public class ProductService {
    private static ArrayList<Product> productList = new ArrayList<>();
    static{
        load();
    }
    public static boolean addProduct(Product product){
        productList.add(product);
        save();
        return true;
    }
    public static boolean delProduct(Product product){
        productList.remove(product);
        save();
        return true;
    }
    public static boolean delProduct(int index){
        productList.remove(index);
        save();
        return true;
    }
    public static boolean delProductAll(){
        productList.removeAll(productList);
        save();
        return true;
    }
    public static ArrayList<Product> getproduct(){
        return productList;
    }
    public static Product getProduct(int index){
        return productList.get(index);
    }
    public static boolean updateProduct(int index,Product product){
        productList.set(index, product);
        save();
        return true;
    }
     public static String calculateTotal(){
        double total = 0;
        for(Product i : productList){
            double price = Double.parseDouble(i.getProductPrice());
            double amount = Double.parseDouble(i.getProductAmount());
            total += price * amount;
        }
        return total+"";
    }
    public static String sumAmount(){
        int total = 0;
        for(Product i : productList){
            double amount = Double.parseDouble(i.getProductAmount());
            total += amount;
        }
        return total+"";
    }
    public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("shoplist.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void load(){
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("shoplist.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
